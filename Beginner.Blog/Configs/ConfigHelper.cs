﻿using Beginner.Blog.Configs.Models;
using Beginner.Blog.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Beginner.Blog.Configs
{
    public class ConfigHelper
    {
        /// <summary>
        /// 获取配置信息
        /// </summary>
        /// <returns></returns>
        public static Setting GetBasicConfig()
        {
            var settingString = FileHelper.ReadFile(WebHelper.GetFilePath("~/Configs/Files/") + "setting.json");
            return JsonConvert.DeserializeObject<Setting>(settingString);
        }
    }
}